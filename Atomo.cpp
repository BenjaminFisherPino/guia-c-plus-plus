#include <iostream>
using namespace std;

#include "Atomo.h"
#include <list>

//CONSTRUCTOR POR DEFECTO
Atomo::Atomo(){
	this->nombre = "S/N";
	this->numero = 0;
}

//CONSTRUCTOR CON PARAMETROS
Atomo::Atomo(string nombre, int numero){
	this->nombre =  nombre;
	this->numero = numero;
}

//METODOS TIPO SETERS
void Atomo::set_nombre(string nombre){
	this->nombre = nombre;
}

void Atomo::set_numero(int numero){
	this->numero = numero;
}

//METODO QUE PERMITE AL PROGRAMA ADJUNTAR UNA COORDENADA
//A UN OBJETO DE TIPO ATOMO EN ESPECIFICO
void Atomo::set_coordenada(){
	cout << "Ingrese las cordenadas del atomo" << endl;
	cout << "X: ";
	cin >> x;
	cout << "Y: ";
	cin >> y;
	cout << "Z: ";
	cin >> z;
	Coordenada cor = Coordenada(x,y,z);
	this->cor = cor;
	}

//METODOS QUE RETORNAN VARIABLES
string Atomo::get_nombre(){
	return this->nombre;
}

int Atomo::get_numero(){
	return this->numero;
}

Coordenada Atomo::get_coor(){
	return this->cor;
	} 
