#include <iostream>
using namespace std;

#include "Coordenada.h"

//CONSTRUCTOR POR DEFECTO
Coordenada::Coordenada(){
	this->x = 0.0;
	this->y = 0.0;
	this->z = 0.0;
	}

//CONSTRUCTOR CON PARAMETROS
Coordenada::Coordenada(float x, float y, float z){
	this->x = x;
	this->y = y;
	this->z = z;
}

//METODOS TIPO SETERS
void Coordenada::set_x(float x){
	this->x = x;
}

void Coordenada::set_y(float y){
	this->y = y;
}

void Coordenada::set_z(float z){
	this->z = z;
}

//METODOS QUE RETORNAN VARIABLES
float Coordenada::get_x(){
	return this->x;
}

float Coordenada::get_y(){
	return this->y;
}

float Coordenada::get_z(){
	return this->z;
}
