#include <iostream>
using namespace std;

#include "Aminoacido.h"
#include "Atomo.h"
#include <list>

//CONSTRUCTOR POR DEFECTO
Aminoacido::Aminoacido(){
	this->nombre = "S/N";
	this->numero = 0;
	}

//CONSTRUCTOR CON PARAMETROS
Aminoacido::Aminoacido(string nombre, int numero){
	this->nombre = nombre;
	this->numero = numero;
}

//METODOS TIPO SETERS
void Aminoacido::set_nombre(string nombre){
	this->nombre = nombre;
}

void Aminoacido::set_numero(int numero){
	this->numero = numero;
}

//FUNCION QUE PERMITE AL PROGRAMA VINCULAR ATOMOS
//A UN AMINOACIDO 
void Aminoacido::add_atomo(){
	cout << "Ingrese la cantidad de atomos del aminoacido" << endl;
	cin >> cantidad;
	
	for (int i = 0 ; i < cantidad ; i++){
		cout << "Ingrese el nombre del atomo" << endl;
		cin >> nombre_atomo;
		cout << "Ingrese el numero del atomo" << endl;
		cin >> numero_atomo;
		Atomo a = Atomo(nombre_atomo, numero_atomo);
		a.set_coordenada();
		this->atomos.push_back(a);
	}
}

//METODOS QUE RETORNAN VARIABLES
string Aminoacido::get_nombre(){
	return this->nombre;
}

int Aminoacido::get_numero(){
	return this->numero;
}

list<Atomo> Aminoacido::get_atomos(){
	return this->atomos;
	}
