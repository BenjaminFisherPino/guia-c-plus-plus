#include <iostream>
using namespace std;

#include <list>
#include "Cadena.h"
#include "Aminoacido.h"

//CONSTRUCTOR POR DEFECTO
Cadena::Cadena(){
	this->letra = "S/L";
}

//CONSTRUCTOR CON PARAMETROS
Cadena::Cadena(string letra){
	this->letra = letra;
}

//METODOS DE TIPO SETERS
void Cadena::set_letra(string letra){
	this->letra = letra;
}

string Cadena::get_letra(){
	return this->letra;
}

//METODO QUE PERMITE LA ADICION DE CADENAS
//A LA PROTEINA
void Cadena::add_aminoacido(){
	cout << "Ingrese la cantidad de aminoacidos de la cadena" << endl;
	cin >> cantidad;
	for (int i = 0 ; i < cantidad ; i++){
	cout << "Ingrese el nombre del aminoacido" << endl;
	cin >> nombre;
	cout << "Ingrese el numero del aminoacido" << endl;
	cin >> numero;
	Aminoacido a = Aminoacido(nombre, numero);
	a.add_atomo();
	this->aminoacidos.push_back(a);
	}
}

//METODO QUE RETORNA UNA VARIABLE
list<Aminoacido> Cadena::get_aminoacidos(){
	return this->aminoacidos;
}
