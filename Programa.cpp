#include <iostream>
using namespace std;

#include "Programa.h"
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"
#include <list>

//CONSTRUCTOR VACIO
Programa::Programa(){}

//METODOS DE LA CLASE PROGRAMA
void Programa::leer_datos_proteinas(){
	for (Proteina p: proteinas){
		cout << "Nombre: " << p.get_nombre() << endl;
		cout << "ID: " << p.get_id() << endl;
	}
}

void Programa::imprimir_datos_proteina(){}

void Programa::agregar_proteina(){
	cout << "Ingrese el nombre de la proteina" << endl;
	cin >> nombre;
	cout << "Ingrese a id de la proteina" << endl;
	cin >> id;
	Proteina p = Proteina(nombre,id);
	cout << "Ingrese la cantidad de cadenas da la proteina" << endl;
	cin >> num_cadena;
	
	//ADICION DE CADENAS
	for (int i = 0; i < num_cadena ; i++){
		p.add_cadena();
	}
	
	this->proteinas.push_back(p);
}

void Programa::imprime_menu(){
	cout << "Ingrese 1 para ver las proteinas disponibles" << endl;
	cout << "Ingrese 2 para imprimir los datos de las proteinas" << endl;
	cout << "Ingrese 3 para agregar una proteina" << endl;
	cout << "Ingrese 4 para salir del programa" << endl << endl;
}

list<Proteina> Programa::get_proteinas(){
	return this->proteinas;
}

//FUNCION MAIN
int main(int argc, char **argv){
	
	//ATRIBUTOS FUNCION MAIN
	list<Proteina> proteinas;
	int resp = 1;
	int opcion;
	
	//INSTANCIA DE CLASES
	Programa p = Programa();
	
	cout << "Bienvenido!" << endl << endl;
	
	//CICLO PRINCIPAL
	while(resp == 1){
		p.imprime_menu();
		cin >> opcion;
		
		//LEER LOS NOMBRES DE LAS PROTEINAS
		if(opcion == 1){
			p.leer_datos_proteinas();
		}
		
		//IMPRIMIR LA INFORMACION DE LAS PROTEINAS
		else if(opcion == 2){
			
			//Este ciclo funciona en base a la lista de proteinas
			//El ciclo accede al primer elemento de la lista y los print
			//van extrayendo la informacion almacenada en estos objetos
			//de tipo proteina, en este caso nombre, id y cadenas.
			
			for (Proteina p:p.get_proteinas()){
				cout << "Nombre: " << p.get_nombre() << endl;
				cout << "ID: " << p.get_id() << endl;
				cout << "Cadenas: " << endl;
				
				//En este ciclo se acceden a las cadenas almacenadas
				//dentro de nuestro objeto de tipo proteina
				
				for (Cadena c : p.get_cadena()){
					cout << "Letra: " << c.get_letra() << endl;
					cout << "Aminoacidos de la cadena" << endl;
					
					//En esta parte se accede a los aminoacidos que
					//estan conectados al objeto cadena
					
					for (Aminoacido a:c.get_aminoacidos()){
						cout << "Nombre: " << a.get_nombre() << endl;
						cout << "Numero: " << a.get_numero() << endl;
						cout << "Atomos del aminoacido" << endl;
						
						//Aqui se esta accediendo a la lista de atomos
						//anclados a un objeto aminoacido
						
						for (Atomo at:a.get_atomos()){
							cout << "Nombre: " << at.get_nombre() << endl;
							cout << "Numero: " << at.get_numero() << endl;
							cout << "Coordenadas " << endl;
							
							//Aqui se accede a la cordenada de cada
							//atomo del aminoacido
							
							cout << "X: " << at.get_coor().get_x() << endl;
							cout << "Y: " << at.get_coor().get_y() << endl;
							cout << "Z: " << at.get_coor().get_z() << endl << endl;
						}
					}
					
				}
			}
		}
		
		//AGREGAR UNA PROTEINA AL PROGRAMA
		else if (opcion == 3){
			p.agregar_proteina();
		}
		
		//SALIR DEL PRGRAMA
		else if (opcion == 4){
			cout << "Hasta luego!" << endl;
			resp = 0;
		}
		
		//OPCION INVALIDA
		else{
			cout << "Opcion invalida..." << endl;
		}
	}
	return 0;
}
