# Archivo README.md

**Versión 1.4**

Archivo README.md para guía numero 1 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 27/08/2021

---
## Resúmen del programa

Este programa fue desarrollado por Benjmaín Fisher para poder almacenar y acceder a informacion de proteinas ingresadas por el ususario. Mediante sencillos pasos uno como usuario puede alamcanar cunatas proteinas uno quiera, cuantas cadenas presentan estas proteinas, cuantos aminoácidos presentan estas cadenas, cuantos atomos presentan cada aminoácido y las coordenadas de cada atomo.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de usuario y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de usuario y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

---
## Como instalarlo

Para instalar el programa debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/guia-c-plus-plus/-/edit/master/README.md

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi usted debe escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

El programa funciona a travez de una funcion Main, la cual permite que el compilador pueda construir y ejecutar el archivo. En esta funcion main se instancia la creacion de un objeto de tipo Programa, este se encarga de mantener la comunicacion entre el usuario y su computadora. Aqui dependiendo de las opciones que ingrese el usuario usted puede, ver los nombres de las proteinas disponibles (opcion 1), ver la informacion de todas las proteinas (opcion 2), agrgar una proteina nueva al sistema (opcion 3) y salir del programa (opcion 4). La opcion 1 nos permite recorrer la lista donde se almacenan las proteinas y accede a sus nombres para ser posteriormente impresos en la terminal. La opcion dos nos permite acceder a toda la informacion disponible de manera ciclica, primero se imprime toda la informacion de una proteina y luego se procede a la siguiente. La opcion 3 nos permite ingresar toda la informacion de una proteina, primero se nos pregunta por el nombre, la id y la cantidad de cadenas de esta proteina. Luego se nos pregunta por las letras de estas cadenas y la cantidad de aminoacidos que cada una de estas cadenas contiene.Luego se nos pregunta por los nombres de estos aminoacidos, el numero de este y la cantidad de atomos que estos poseen. Por ultimo se nos pregunta por el nombre del atomo, su numero y sus coordenadas. La opcion 4 nos permite salir del bucle principal y proceder a terminar el programa.

Espero que este archivo README.md les haya ayudado a resolver sus dudas generales respecto al programa. En caso de tener más dudas la direccion de correo electroncio al que se pueden dirigir es bfisher20@alumnos.utalca.cl.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

Gracias por utilizar este programa de codigo abierto!
