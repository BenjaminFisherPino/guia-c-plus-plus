#include <iostream>
using namespace std;

#include "Proteina.h"
#include "Cadena.h"
#include <list>

//CONSTRUCTOR POR DEFECTO
Proteina::Proteina(){
	this->nombre = "S/N";
	this->id = "S/ID";
	}

//CONSTRUCTOR CON PARAMETROS
Proteina::Proteina(string nombre, string id){
	this->nombre = nombre;
	this->id = id;
}

//METODOS  DE TIPO SETERS
void Proteina::set_nombre(string nombre){
	this->nombre = nombre;
}

void Proteina::set_id(string id){
	this->id = id;
}

//METODO QUE PERMITE LA ADICION DE
//CADENAS A LA PROTEINA
void Proteina::add_cadena(){
	cout << "Ingrese la letra de la cadena" << endl;
	cin >> letra;
	Cadena c = Cadena(letra);
	c.add_aminoacido();
	this->cadenas.push_back(c);
}

//METODOS QUE RETORNAN VARIABLES
string Proteina::get_nombre(){
	return this->nombre;
}

string Proteina::get_id(){
	return this->id;
}

list<Cadena> Proteina::get_cadena(){
	return cadenas;
}
