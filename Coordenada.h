#ifndef COORDENADA_H
#define COORDENADA_H

class Coordenada {

    //ATRIBUTOS
    private:
		float x;
		float y;
		float z;

    public:
    //CONSTRUCTOR POR DEFECTO
        Coordenada();
                
    //CONSTRUCTOR CON PARAMETROS
		Coordenada(float x, float y, float z);

    //METODOS
    
		//SETERS
		void set_x(float x);
		void set_y(float y);
		void set_z(float z);
		
		//GETERS
		float get_x();
		float get_y();
		float get_z();
};
#endif
