#ifndef AMINOACIDO_H
#define AMINOACIDO_H

#include "Atomo.h"
#include <list>

class Aminoacido {

    //ATRIBUTOS
    private:
		string nombre, nombre_atomo;
		int numero, numero_atomo, cantidad;
		list<Atomo> atomos;

    public:
    //CONSTRUCTOR POR DEFECTO
        Aminoacido();
        
    //CONSTRUCTOR CON PARAMETROS
		Aminoacido(string nombre, int numero);
		
    //METODOS
    
		//SETERS
		void set_nombre(string nombre);
		void set_numero(int numero);
		void add_atomo();
		
		//GETERS
		string get_nombre();
		int get_numero();
		list<Atomo> get_atomos();
};
#endif
