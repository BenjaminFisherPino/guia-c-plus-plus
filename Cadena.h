#ifndef CADENA_H
#define CADENA_H

#include <list>
#include "Aminoacido.h"

class Cadena {

    //ATRIBUTOS
    private:
		int numero, cantidad;
		string letra, nombre;
		list<Aminoacido> aminoacidos;
		
    public:
    //CONSTRUCTOR POR DEFECTO
        Cadena();
    
    //CONSTRUCTOR CON PARAMETROS
        Cadena(string letra);
        
    //CONSTRUCTOR CON PARAMETROS
        
    //METODOS
    
		//SETERS
		void set_letra(string letra);
		void add_aminoacido();
		
		//GETERS
		string get_letra();
		list<Aminoacido> get_aminoacidos();


};
#endif
