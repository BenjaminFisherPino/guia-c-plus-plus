#ifndef PROTEINA_H
#define PROTEINA_H

#include "Cadena.h"
#include <list>

class Proteina {

    //ATRIBUTOS
    private:
        string nombre, letra;
        string id;
        list<Cadena> cadenas;
        
    public:
    //CONSTRUCTOR POR DEFECTO
        Proteina();
        
    //CONSTRUCTOR CON PARAMETROS
        Proteina(string nombre, string id);
        
    //METODOS
    
		//SETERS
		void set_nombre(string nombre);
		void set_id(string id);
		void add_cadena();
		
		//GETERS
		string get_nombre();
		string get_id();
		list<Cadena> get_cadena();
};
#endif
