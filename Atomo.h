#ifndef ATOMO_H
#define ATOMO_H

#include "Coordenada.h"

class Atomo {

    //ATRIBUTOS
    private:
		string nombre;
		int numero;
		Coordenada cor;
		float x, y, z;

    public:
    //CONSTRUCTOR POR DEFECTO
        Atomo();
                
    //CONSTRUCTOR CON PARAMETROS
		Atomo(string nombre, int numero);

    //METODOS
		
		//SETERS
		void set_nombre(string nombre);
		void set_numero(int numero);
		void set_coordenada();
		
		//GETERS
		string get_nombre();
		int get_numero();
		Coordenada get_coor();
};
#endif
