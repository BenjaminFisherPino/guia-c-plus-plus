#ifndef PROGRAMA_H
#define PROGRAMA_H

#include "Proteina.h"
#include <list>

class Programa {

    //ATRIBUTOS
    private:
        list<Proteina> proteinas;
        string nombre, id, letra;
        int num_cadena;
        
    public:
    //CONSTRUCTOR POR DEFECTO
        Programa();
        
    //METODOS
		void leer_datos_proteinas();
		void imprimir_datos_proteina();
		void agregar_proteina();
		void imprime_menu();
		list<Proteina> get_proteinas();
};
#endif
